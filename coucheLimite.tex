\documentclass[french,10pt,a4paper]{article}
\usepackage[]{enseignement}
\usepackage{color}
\usepackage{enumitem}


\usepackage{minted}


\renewcommand{\t}[1]{\tilde{#1}}

\newenvironment{reponseCode}{%
  
}{%
  
}

\newcounter{saveenum}
\setcounter{saveenum}{0}

\newcommand{\question}[1]{{\color{red} \begin{enumerate}[label=\#\arabic*. ] \setcounter{enumi}{\value{saveenum}} \item #1  \setcounter{saveenum}{\value{enumi}}\end{enumerate} }}
\definecolor{anti-flashwhite}{rgb}{0.95, 0.95, 0.96}

\usemintedstyle{colorful}
\setminted[python]{fontsize=\footnotesize,bgcolor = anti-flashwhite, frame=lines, framesep=2mm, autogobble = true}
\setminted[console]{fontsize=\footnotesize,bgcolor = black, frame = single}


\begin{document}		


	
 %%%%%%%%%%%%%%%%%%%%%%%%% TITRE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\matiere{Simulation pour les fluides et la thermique}
\institut{ENSE3}
\title{TD 2. Convection-diffusion en 2D}
\auteur{H. Bodiguel, Z. Huang, G. Balarac, R. Barbera}
\annee{2023-2024}
\maketitle
%%%%%%%%%%%%%%%%%%%%%%%%%% EXERCICES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

\subsection{Enoncé du problème physique}

On considère un écoulement unidirectionnel d'un fluide à vitesse $\mathbf{v}=v_0 \mathbf{e}_x$, et température $T_0$. Pour $x>0$, le fluide est au contact d'une plaque, maintenue à la température $T_1$. 

On supposera que le fluide peut glisser sur la paroi, et par suite que le champs de vitesse est uniforme, restant égal à $v_0 \mathbf{e}_x$. Cette hypothèse correspond au cas où le nombre de Prandtl est petit par rapport à l'unité, ce qui est le cas des métaux liquides par exemple. On supppose que les propriétés physiques du fluides (conductivité, masse volumique et chaleur spécifique) sont constantes. 

La paroi échange de la chaleur avec le fluide, et il se développe une "couche limite" thermique, définie comme la région de l'espace au voisinage de la plaque où la température varie de $T_1$ proche de la plaque à $T_0$.



\begin{center}
	\includegraphics[width=0.50\textwidth]{scheme.png}
\end{center}

Cette couche limite est d'une gande importance pratique. En effet, c'est elle qui conttôle le flux de chaleur échangé entre la plaque et le fluide. En tout point de la plaque, il est en effet donné par 

\begin{equation}
\mathbf{q} = - \lambda \partial_y T \mathbf{e}_y
\end{equation}
Il faut donc pouvoir résopudre le champ de température dans le fluide pour déterminer ce flux de chaleur. 

\subsection{Mise en équation}

L'équation de la chaleur s'énonce comme suit 
\begin{equation}
\rho c_p \left(\partial_t T + \mathbf{v}\cdot \mathbf{\nabla} T \right)= \lambda\Delta T
\end{equation}

Compte tenu des hypothèses du problème ($\mathbf{v}$ uniforme, invariance par translation suivant $\mathbf{e}_z$), l'équation se simplifie aisément :

\begin{equation}
\rho c_p \left(\partial_t T + v_0 \partial_x T \right)= \lambda \left(\partial_{xx} + \partial_{yy}\right)T
\end{equation}

\question{En introduisant les variables adimensionnées suivantes : $\theta=\left(T_1-T_0\right)/T_1$, $\t{t}= t \alpha /L^2$ , $\t{x}=x/L$, $Pe=v_0 L/\alpha$ où $\alpha = \lambda/\rho c_p$, montrer que l'équation se simplifie ainsi : 
\begin{equation}
\partial_{\t{t}} \theta + \partial_{\t{x}} \theta = \frac{1}{Pe}\lambda \left(\partial_{\t{x}\t{x}} + \partial_{\t{y}\t{y}}\right) \theta
\label{eq:chaleurAdim}
\end{equation}}

\subsection{Objectif}

L'objectif est de résoudre cette équation, de déterminer le champs de température \textit{en régime stationnaire} et d'en déduire le flux de chaleur $\phi$ transféré au fluide, par unité de longueur dans la direction transverse. Ce dernier est donné par 
\begin{equation}
\phi = \int_0^L -\lambda \partial_y T dx = - \lambda \left(T_1-T_0\right) \int_0^1 \partial_{\t{y}} \theta d\t{x} ,
\end{equation}
Le nombre de Nusselt représente ce flux adimnesionné, à savoir :
\begin{equation}
\text{Nu} = - \int_0^1 \partial_{\t{y}} \theta d\t{x}
\label{eq:Nudef}
\end{equation}
On définit également un nombre de Nusselt local, qui permet de déterminer le flux localement :
\begin{equation}
\text{Nu}_x = - \partial_{\t{y}} \theta 
\label{eq:Nudef}
\end{equation}

\subsection{Solution analytique approchée}

En négigeant la dérivée seconde par rapport à $x$, et en régime stationnaire, l'équation se résume à :
\begin{equation}
\partial_{\t{x}} \theta = \frac{1}{Pe}\lambda \partial_{\t{y}\t{y}}\theta
\end{equation}
Cette équation possède une solution qui satisfait la condition de température imposée en $y=0$, donnée par 
\begin{equation}
\theta=1-\text{erf}\left(\frac{\t{y}}{2\sqrt{\t{x}/Pe}}\right)
\label{eq:solapprochee}
\end{equation}
Ce résultat est intéressant car il va nous aider à : 
\begin{itemize}
	\item dimensionner le domaine de calcul. 
	\item Estimer le temps nécessaire pour la convergence.
\end{itemize}
Il permet également de calculer le nombre de Nusselt. On trouve en utilisant l'équation \ref{eq:Nudef} que 
\begin{equation}
	\text{Nu} = 2 \sqrt{\text{Pe}/\pi}
\label{eq:Nuth}
\end{equation}

\question{En vous basant sur cette solution, proposer un dimensionnement du domaine de calcul. Par simplicité, on ravaillera avec un domaine rectangulaire.}
\reponse{}{La distance caractéristique sur laquelle varie la température est donnée par $ \delta = \sqrt{\t{x}/\text{Pe}} $. Pour $\t{y} \gg \delta $, la température est uniforme et égale à $T_0$ (soit $\theta = 0$). On peut donc choisir un domaine de calcul de largeur $1$ (en $\t{x}$) et de hauteur $k\text{Pe}^{-1/2}$, où $k$ vaut typiquement quelques unités. }

\question{Donner l'ordre de grandeur du temps caractéristique impliqué dans le régime transitoire. }
\reponse{}{La chaleur doit diffuser sur une distance $\delta$. Si on appelle $\tau$ le temps caractéristique, l'équation \ref{eq:chaleurAdim} s'écrit en ordre de grandeur : 
 \begin{equation}
\frac{1}{\tau} \sim \frac{1}{\text{Pe}} \delta^2 
 \label{eq:}
 \end{equation}
Donc $\tau \sim 1$ 
}

\section{Mise en place du schéma numérique}

On veut discrétise l'équation \ref{eq:chaleurAdim} en utilisant les volumes finis. On défini un maillage du domaine ([0 1],[0 $L_y$]) basé sur des cellules rectangulaires de taille $\Delta x \times \Delta y$, numérotés de 0 à $N_x-1$ et de 0 à $N_y-1$. Les noeuds sont pris au centre des cellules, et on note $\theta_{i,j}^n$ la valeur de $\theta$ sur le noeuds $(i,j)$ au temps $t=n\Delta t$ On  utilisera un schéma de Euler explicite pour la discrétisation temporelle, un schéma centré pour les flux diffusifs, et, afin d'obtenir un schéma stable, un schéma décentré amont pour les flux convectifs.  


\question{Pour une cellule dans le corps du domaine (pas sur les frontières), expliciter chacun des flux $\phi_N$, $\phi_S$, $\phi_W$, $\phi_E$ (les flux sur chacune des arrètes d'une cellule. }

\reponse{}{
\begin{equation}
\phi_N= \frac{1}{\text{Pe}}\frac{\Delta x}{\Delta y}(\theta_{i,j+1}^n-\theta_{i,j}^n)
\end{equation}
\begin{equation}
\phi_S= \frac{1}{\text{Pe}}\frac{\Delta x}{\Delta y}(\theta_{i,j-1}^n-\theta_{i,j}^n)
\end{equation}
\begin{equation}
\phi_W= \Delta y \theta_{i-1,j}^n + \frac{1}{\text{Pe}}\frac{\Delta y}{\Delta x}(\theta_{i-1,j}^n-\theta_{i,j}^n)
\end{equation}
\begin{equation}
\phi_E= - \Delta y \theta_{i,j}^n + \frac{1}{\text{Pe}}\frac{\Delta y}{\Delta x}(\theta_{i+1,j}^n-\theta_{i,j}^n)
\end{equation}

}

\question{En intégrant l'équation \ref{eq:chaleurAdim} sur une maille, établir l'expression de $\theta_{i,j}^{n+1}$ en fonction de $\theta_{i,j}^n$, $\Delta x$, $\Delta y$, $\Delta t$ et des flux établis question précédente. Cette relation de récurrence correspond à une itération. Elle doit être appliquée sur toute les cellules à chaque itération.  }

\reponse{}{

\begin{equation}
            \theta_{i, j}^{n+1} = \theta_{i, j}^n + \frac{\Delta t}{\Delta x\Delta y}\left(\phi_N+\phi_S+\phi_W+\phi_E\right)
\end{equation}
}

Pour obtenir le schéma numérique complet, il nous reste à exliciter les flux au niveau des frontières (les conditions aux limites). On prendra comme condition : 
\begin{itemize}
	\item température imposée en $y=0$ : $\theta = 1$.  
	\item température imposée en $x=0$ : $\theta = 0$.  
	\item Flux diffusif nul en $x=1$.
	\item Flux diffusif nul en $y=L_y$.
\end{itemize}
Remarque : sauf en $y=0$, les conditions aux limites ne sont pas rigoureuses physiquement, puisque le problème physique a des conditions aux limites à l'infini... 

\question{Réécrire les flux $\phi_N$, $\phi_S$, $\phi_W$, $\phi_E$ pour l'ensemble cellules situées sur les frontières du domaine. Attention à ne pas faire intervenir dans ces expressions de noeuds en dehors du domaine. }

\reponse{}{
$$\text{si} \quad j=N_y, \quad \phi_N= 0$$

$$\text{si} \quad j=0, \quad \phi_S= \frac{2}{\text{Pe}}\frac{\Delta x}{\Delta y}(1-\theta_{i,j}) $$

$$\text{si} \quad i=0, \quad \phi_W= -\frac{2}{\text{Pe}}\frac{\Delta y}{\Delta x}\theta_{i,j}$$

$$\text{si} \quad i=N_x, \quad \phi_E= -\Delta y \theta_{i,j} $$

}

L'ensemble des expressions ci-dessus définissent un schéma numérique complet, à condition de rajouter une condition initiale. On choisir par simplicité une valeur nulle dans tout le domaine à $t=0$. 

\section{Un premier code intuitif}

Nous allons implémenter un code simple en Python capable de résoudre le problème, en utilisant les paramètres suivants :
\begin{itemize}
	\item $N_x = 60$
	\item $N_y = 30$
	\item $\text{Pe}=10^3$
	\item $L_y = 4 \cdot  \text{Pe}^{-1/2}$
	\item $\Delta t = 5\times 10^{-3}$
\end{itemize} 

Le programme devra en outre :
\begin{itemize}
	\item Calculer le nombre de Nusselt $\text{Nu}_{n}$ à chaque itération.
	\item Calculer les résidus à chaque itérations, définis comme 
		$$\text{res}_{n+1} = \sqrt{\sum_{i,j}{\left(\theta_{i,j}^{n+1}-\theta_{i,j}^{n}\right)^2} } $$
	\item Toutes les $\Delta t_{\text{disp}}$, afficher le nombre d'itérations, les résidus, le nombre de Nusselt. En pratique, on prendra $\Delta t_{\text{disp}}=0.1$. 
	\item Itérer jusqu'à $t=T_{max}$. On prendra $T_{max}=2$. 
	\item Stopper le calcul si l'écart relatif entre le résultat de deux itérations successive est inférieur à un critère de convergence, défini à $10^{-12}$. 
\end{itemize} 



\question{Compléter le code suivant afin de respecter ce cahier des charges.} 

\begin{minted}{python}
import numpy as np
import matplotlib.pyplot as plt
from scipy import special

# Paramètres du problème
Lx = 1.0	# Longueur du domaine en x
T_max = 2	# Temps total
precision=1e-12	#critere de convergence
dt_Disp=0.1	# Nombre de points temporels avant affichage
Nx = 60		# Nombre de points spatiaux en x
Ny = 30 	# Nombre de points spatiaux en y
Pe = 1000 # Pe
nLy=4
Ly = nLy/np.sqrt(Pe) 	# Longueur du domaine en y
dx = XXX
dy = XXX
# pas de temps
dt= 5e-3 
Ndisp= XXX	# Nombre d'itérations entre deux affichages 
Nt= XXX	# Nombre d'itérations max
# Initialisation de la grille
x = XXX  # vecteur x (cell centered)
y = XXX  # vecteur x (cell centered)
X, Y = np.meshgrid(x, y) # matrices des positions x et y
u = np.zeros((Nx, Ny)) # matrice resultat
res = np.zeros(Nt) # vecteur permettant de stocker les residus à chaque itération
Nu=np.zeros(Nt) # vecteur permettant de stocker le Nu a chaque iteration
x_full=np.concatenate((np.array([0]),x,np.array([1]))) # x incluant les frontières (pour calcul de Nu). 
# Conditions initiales
u[:,:] = 0
# Schema numerique
for n in range(Nt):
	##### IMPLEMENT YOUR NUMERICAL SCHEME HERE #####
	TODO
	#### 
	res[n]= XXX
	Nu[n]= XXX
	# affichage des iterations
	if (n>0 and n % Ndisp == 0):
		print('it %g, t=%g, residu=%g, Nu=%g' % (n, n*dt, res[n], Nu[n]))
	# test de la convergence
	if XXX :
		print('Convergence is reached')
		break
Nu=Nu[Nu>0]  # supprime les éléments en trop du vecteur Nu
res=res[res>0] # supprime les éléments en trop du vecteur res
\end{minted}

Vous devriez obtenir le résultat suivant :

\begin{minted}{console}
it 20, t=0.1, residu=0.00408877, Nu=61.7865
it 40, t=0.2, residu=0.00229191, Nu=47.9538
it 60, t=0.3, residu=0.00157257, Nu=42.5295
it 80, t=0.4, residu=0.00116275, Nu=39.7264
it 100, t=0.5, residu=0.000885971, Nu=38.1091
it 120, t=0.6, residu=0.000677017, Nu=37.1342
it 140, t=0.7, residu=0.000503834, Nu=36.5468
it 160, t=0.8, residu=0.000346595, Nu=36.2102
it 180, t=0.9, residu=0.000200656, Nu=36.0398
it 200, t=1, residu=8.79869e-05, Nu=35.97
it 220, t=1.1, residu=2.75832e-05, Nu=35.9486
it 240, t=1.2, residu=6.17722e-06, Nu=35.9437
it 260, t=1.3, residu=1.01808e-06, Nu=35.9429
it 280, t=1.4, residu=1.28334e-07, Nu=35.9427
it 300, t=1.5, residu=1.28443e-08, Nu=35.9427
it 320, t=1.6, residu=1.05489e-09, Nu=35.9427
it 340, t=1.7, residu=7.31162e-11, Nu=35.9427
Convergence is reached
\end{minted}

\reponse{}{Correction : voir jupyter notebook}

\question{Tracer l'évolution des résidus en fonction des iterations (figure ci-dessous).}

\begin{minted}{python}
fig = plt.figure()
ax = fig.add_subplot(111)
plt.semilogy(res)
ax.set_xlabel('iterations')
ax.set_ylabel('residus')
plt.show()
\end{minted}

\begin{center}		\includegraphics[width=0.60\textwidth]{residus.png} \end{center}

Le fait que les résidus tendent vers zero permet de s'assurer que le calcul converge vers la solution stationnaire. On constate que les résidus diminuent très rapidment après 200 itérations, c'est à dire pour $\t{t}>1$. Il s'agit du temps caractéristique du régime stationnaire. 

\question{Même question pour le nombre de Nusselt.}

\begin{center}\includegraphics[width=0.60\textwidth]{Nusselt.png} \end{center}


\question{Représenter la solution obtenue comme une image en fausse couleurs. Y superposer l'allure de la couche limite obtenue grâce à la solution approchée ($\t{y}= \sqrt{\t{x}/\text{Pe}}$).}

\begin{minted}{python}
fig = plt.figure()
ax = fig.add_subplot(111)
im=ax.imshow(u.T, origin='lower',aspect='auto',extent=(0,1,0,Ly))
ax.plot(x,np.sqrt(x/Pe),'r--')
ax.set_xlabel('x')
ax.set_ylabel('y')
plt.colorbar(im)
ax.set_title('solution numérique')
plt.show()
\end{minted}

\begin{center}		\includegraphics[width=0.60\textwidth]{solution.png} \end{center}

\question{On cherche à comparer la solution théorique (approchée) et la solution numérique. Déterminer l'erreur relative entre le nombre de Nusselt théorique et le nombre de Nusselt calculé numériquement : $\left|\text{Nu}_{\text{num}}-\text{Nu}_\text{th}\right|/\text{Nu}_\text{th}$.  }

\begin{minted}{console}
0.03636007341636877
\end{minted}

On obtient une erreur de 3.6\%, ce qui n'est pas négligeable. Pour comprendre l'origine de cette erreur, nous allons déterminer le nombre de Nusselt local. 

\question{Représenter le nombre de Nusselt local en fonction de $\t{x}$, pour la solution numérique et pour la solution théorique. Commenter.}

\begin{center}		\includegraphics[width=0.60\textwidth]{NuLocal.png} \end{center}

On constate que la différence est importante proche de $x=0$, le calcul numérique donne un flux légèrement plus important au voisinage de zéro.  

\question{Représenter sous la forme d'une image en fausse couleur la différence entre la solution théorique et la solution analytique. }

\begin{center}		\includegraphics[width=0.60\textwidth]{comparaisonTheo.png} \end{center}

Ici également, on constate que l'erreur est importante surtout au voisinage de $x=0$... 
Il y a deux explications possibles :
\begin{itemize}
	\item La solution théorique résulte d'une approximation. Or celle-ci n'est pas forcément valide proche de $x=0$, puisque les gradients de température sont importants. 
	\item La solution numérique a nécessairement une erreur de troncature, qui est possiblement plus grande en $x=0$, car les gradients de températures sont importants. 
\end{itemize}

Nous allons donc devoir procéder à une étude complète de la précision du résultat numérique. 


\section{Etude de la précision}

\subsection{Erreur de troncature théorique}

\question{Déterminer l'erreur de troncature $\epsilon$ du schéma numérique utilisé. On se limitera au premier ordre non nul en $\Delta t$, $\Delta x $ et en $\Delta y$. En déduire que le schéma utilisé est d'ordre 2 en $y$, d'ordre 1 en $x$. }


\subsection{Un code plus performant}

Afin de vérifier cette erreur de troncature numériquement, nous allons effectuer une convergence en maillage, c'est à dire faire varier les pas d'espace et déterminer l'erreur numérique systématiquement. Pour caculer l'erreur, il est plus facile de disposer d'une solution asymptotique. Nous allons donc prendre des flux diffusifs nuls suivant $x$ pour effectuer la comparaison. 

L'étude de la convergence en maillage n'est pas possible avec le code précédent, qui n'est pas optimisé et qui conduirait à des temps de calculs trop longs. On fournit donc le fichier \texttt{coucheLimite.py} dans laquelle le calcul a été optimisé pour Python. La principale différence réside dans le fait qu'un calcul matriciel est utilisé à chaque itération, en utilisant des matrices creuses. Au passage, le code a aussi été transformé, et définit une fonction pour plus de versatilité.    

\begin{minted}[breaklines=true]{python}
def coucheLimite(Nx=60,Ny=30,Pe=1000,nLy=4,diffx=1,display=1,dt=0,dt_Disp=0.1, precision=1e-12, T_max=2, coef_stab_c=0.3, coef_stab_d=0.3):
    ### ARGUMENTS ###
    # Nx : nombre de points en x
    # Ny : nombre de points en y
    # Pe : nombre de Peclet
    # nLy : prefacteur permettant de dimensionner la taille du domaine en y : Ly = nLy/sqrt(Pe)
    # diffx : diffusion suivant x (si 1) ou non (si 0)
    # display : affichage de graphiques si display = 2, des iterations si display = 1, de rien du tout si display = 0
    # dt_Disp : pas de temps pour l'affichage des iterations
    # precision : limite en precision pour arreter la convergernce 
    # T_max : temps maximum de la simulation
    # coef_stab_c : coefficient pour la stabilité par rapport à la convection
    # coef_stab_d : coefficient pour la stabilité par rapport à la diffusion
	[...]
	return err, x, y, u, dy, dx, Nuf, res
\end{minted}


\question{Vérifiez que les deux codes sont parfaitement équivalents, en reprenant les conditions du code utilisé précédemment.}

\begin{minted}{python}
err, x, y, u, dy, dx, Nuf, res =coucheLimite(Nx=60,Ny=30,Pe=1000,dt_Disp=0.1, display=1,dt=0.005)
\end{minted}

\question{Afin d’optimiser le temps de calcul, on va essayer également de trouver le pas de temps optimal. Puisqu’on s’intéresse à la solution stationnaire, le pas de temps ne devrait pas avoir d’impact sur la précision. Vérifiez-le en comparant l'erreur obtenue avec \texttt{dt=0.005} et \texttt{dt=0.00005}}

\begin{minted}{console}
dt = 0.005
erreur : 0.0107718
dt = 0.00005
erreur : 0.0107718
\end{minted}

Au passage, vous pourrez remarquez que les calculs intermédiaires ne sont pas équivalents. L'erreur numérique sur la solution instationnaire est plus grande en principe si le pas de temps est plus élevé. Pour la solution stationnaire, cela n'a aucun impact, pour peu que le schéma soit stable. 

\subsection{Stabilité}

On utilise ici un schéma explicite en temps, qui devient instable si le pas de temps est trop grand. 

\question{Illustrer la non-stabilité du schéma et déterminer la limite supérieure du pas de temps, toujours pour $Nx=60,Ny=30$.}

\begin{center}\includegraphics[width=\textwidth]{instable.png} \end{center}

L'étude de la stabilité (non traitée dans ce sujet) montre que le schéma devient instable soit à cause des flux diffusifs soit à cause des flux convectifs, en fonction des paramètres de la simulation (Pe, $\Delta x$, $\Delta y$, et $\Delta t$). On aboutit à la condition de stabilité suivante : 

\begin{equation}
	\delta t < \text{min}\left( k_c \Delta \t{x}, k_d \text{Pe}\left(\frac{1}{\Delta \t{x}^2}+\frac{1}{\Delta \t{y}^2}\right)^{-1}\right), 
\label{eq:stab}
\end{equation} 
où $k_c$ et $k_d$ sont des constantes proches de l'unité. 


\question{Déterminer empiriqement les valeurs $k_c$ et $k_d$, sachant que l'on souhaite faire varier le nombre de Peclet entre 1 et $10^5$. }
\reponse{}{On trouve empiriquement $k_c = 0.33$ et $k_d=0.4$}

Dans tout ce qui suit, on travaillera avec le pas de temps le plus grand possible, afin de limiter les temps de calcul. 


\subsection{Convergence en maillage}

Dans cette partie, nous allons estimer l'erreur en comparant la solution obtenue à la solution exacte. Comme celle-ci n'existe que dans l'approximation où la diffusion suivant $x$ est négligée, nous allons la prendre nulle dans toute cette partie (on utilisera pour cela l'argumeent \texttt{diffx} de la fonction \texttt{coucheLimite}). 

\question{Effectuez une étude de convergence en maillage, à la fois sur $N_x$ et sur $N_y$. Tracer l'erreur en fonction de $N_x$ pour deux valeurs de $N_y$, et comparer là à l'erreur théorique.	On choisira une taille de domaine de $3 \text{Pe}^{-1/2}$}

Vous devriez obtenir le résultat suivant : 

\begin{center}
	\includegraphics[width=0.6\textwidth]{precision_1.png}
\end{center}

On remarque que l'erreur décroit en fonction de $N_x$ mais plus faiblement qu'attendu théoriquement. On remarque par ailleurs que $N_y$ n'influence pas l'erreur. Ces deux observations laissent penser que l'erreur est contrôlée par une autre approximation. Cela pourrait provenir de la taille du domaine. 

\question{Pour différentes valeurs de $N_x$, déterminer et tracer l'erreur en fonction de la taille du domaine. Afin de ne pas être sensible à l'erreur due à $N_y$, on prendra une valeur assez grande pour ce paramètre (200 devrait suffire). En déduire la taille minimale du domaine permettant de négliger cette source d'erreur. }

\begin{center}
	\includegraphics[width=0.6\textwidth]{precisionTailleDomaine.png}
\end{center}

\reponse{}{Il faut prendre une taille minimale de $5Pe^{-1/2}$ pour pouvoir négliger l'erreur du à la taille finie du domaine.}


\question{Effectuer une convergence en maillage en $N_y$, pour différentes valeur de $N_x$, et montrer que l'on obtient une convergence d'ordre 2, à condition que l'erreur due à $N_x$ soit négligeable. }

\begin{center}
	\includegraphics[width=0.6\textwidth]{precisionNy.png}
\end{center}

\question{Effectuer une convergence en maillage en $N_x$, pour différentes valeur de $N_y$, et montrer que l'on obtient une convergence d'ordre 1, à condition que l'erreur due à $N_y$ soit négligeable. }

\begin{center}
	\includegraphics[width=0.6\textwidth]{precisionNx.png}
\end{center}

\question{Déterminer la relation entre $N_x$ e $N_y$ permettant d'optimiser le temps de calcul. }

\reponse{}{L'erreur est approximativement donnée par : $\epsilon \simeq 
4/N_y^2+ 0.4/N_x$. Le calcul est optimal si les deux termes sont égaux, puisque dans le cas contraire l'erreur sera dominée par l'un ou par l'autre. On pourra donc chosir $N_y = \sqrt{10 Nx}$. }


\question{Effectuer une convergence en maillage sur le calcul de Nu, en utilisant la relation précédemment établie. Dans cette question, on s'intéresse également au temps de calcul. Tracer en fonction de $N_x$, l'erreur relative sur le nombre de Nusselt (en le comparant au Nusselt théorique) et le temps de calcul. Le code suivant permet de définir les fonctions tic() et toc() qui mesurent le temps de calcul. }

\begin{minted}{python}

def tic():
    import time
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()

def toc():
    import time
    if 'startTime_for_tictoc' in globals():
        total_time=time.time() - startTime_for_tictoc
        print("Elapsed time is " + str(total_time) + " seconds.")
        return total_time
\end{minted}


\begin{center}
	\includegraphics[width=0.6\textwidth]{precisionNu.png}
\end{center}

On remarque que si l'erreur diminue en 1/$N_x$, le temps de calcul augmente lui beaucoup plus rapidement: les calculs précis sont très couteux !!!


\section{Précision en l'absence de solution exacte}

Dans cette partie, nous allons considérer la diffusion suivant $x$. Il n'existe plus de solution exacte, et il ne sera pas possible de déterminer l'erreur à priori. 


\question{Tracer en fonction de Pe le nombre de Nusselt et le comparer au résultat obtenu sans diffusion suivant $x$}
\begin{center}
	\includegraphics[width=0.6\textwidth]{NuVSPe1.png}
\end{center}
La comparaison de ces résultats avec la littérature n'est pas concluante, le nombre de Nusselt calculée est trop élevé, en particulier à bas Péclet. Qui plus est, l'effet de la diffusion suivant $x$ n'est pas négligeable même à des Péclet de 100, ce qui n'est pas attendu. En fait, il s'agit d'une erreur de modélisation associé au fait d'avoir pris un domaine uniquement resreint entre 0 et 1. Or, la diffusion suivant $x$ génére un flux dans la direction $x$ vers l'aval qui n'est pas négligeable à faible nombre de Peclet. Les conditions aux limites imposés en $x=0$ ne sont donc pas physiques, et et il faut étendre un peu le domaine de calcul vers $x<0$. 

\question{Créer une nouvelle fonction à partir de la fonction \texttt{coucheLimite} au sein de laquelle le domaine de calcul est étendu : $\t{x} \in [-0.25, 1]$. La plaque est toujours entre 0 et 1. Illustrer la différence entre les deux modèles, pour Pe=10, et comparer les nombre de Nusselt. }

\begin{center}
	\includegraphics[width=0.7\textwidth]{DomaineEtendu.png}
\end{center}


\question{Retracer en fonction de Pe le nombre de Nusselt en utilisant le domaine étendu et le comparer au résultat obtenu sans diffusion suivant $x$}
\begin{center}
	\includegraphics[width=0.6\textwidth]{NuVSPe2.png}
\end{center}

Ces résultats sont cette fois en accord avec la littérature. 

\question{Afin d'évaluer la précision du calcul, effectuer une convergence en maillage pour $\text{Pe}=1000$. On utilisera toujours la relation optimisée entre $N_x$ et $N_y$. }

\begin{center}
	\includegraphics[width=0.6\textwidth]{ConvergenceNuDiffx.png}
\end{center}

On constate que l'on tend asymptotiquement vers une valeur. Pour déterminer cette valeur asymptotique et ainsi estimer la précision du calcul, nous allons ajuster la courbe précédente par une fonction du type $\text{Nu}=\text{Nu}_\infty - a/Nx$. On sait en effet que la convergence est d'ordre 1. 

\question{A l'aide de la fonction \texttt{curve\_fit} du module \texttt{scipy.optimize}, ajuster les données obtenues précédemment et donner la valeur de $\text{Nu}_\infty$. }

\begin{center}
	\includegraphics[width=0.6\textwidth]{ConvergenceAjustee.png}
\end{center}


\section{Pour aller plus loin}

Le code utilisé dans ce sujet permet moyennant de très petits ajustement de traiter aussi le problème de transfert dans une géométrie de type canal entre deux plaques. Il est également possible de remplacer la condition de température imposée par une condition de flux imposé. 

On obtient les résultats suivants. 

A température imposée :
\begin{center}
	\includegraphics[width=0.8\textwidth]{CanalTImpose.png}
\end{center}


A flux imposé :
\begin{center}
	\includegraphics[width=0.8\textwidth]{CanalFluxImpose.png}
\end{center}

\end{document}

Etablir le shéma numérique suivant : Euler explicite, schéma centré. 

$$ \Delta x \Delta y \left(\theta_{i,j}^{n+1} - \theta_{i,j}^{n}\right) = \Delta t \left[ \Delta_y \left(\theta_{i-1/2,j}^n - \theta_{i+1/2,j}^n\right) 
	+ \frac{\Delta y}{Pe \Delta x } \left(\theta_{i-1,j}^n - 2\theta_{i,j}^n + \theta_{i+1,j}^n\right)    + \frac{\Delta x}{Pe \Delta y } \left(\theta_{i,j-1}^n - 2\theta_{i,j}^n + \theta_{i,j+1}^n\right) \right]
$$

$$ \theta_{i,j}^{n+1} =  \theta_{i,j}^{n} + \Delta t \left[ \frac{1}{2\Delta x} \left(\theta_{i-1,j}^n - \theta_{i+1,j}^n\right) 
	+ \frac{1}{Pe \Delta x^2 } \left(\theta_{i-1,j}^n - 2\theta_{i,j}^n + \theta_{i+1,j}^n\right)    + \frac{1}{Pe \Delta y^2 } \left(\theta_{i,j-1}^n - 2\theta_{i,j}^n + \theta_{i,j+1}^n\right) \right]
$$

Conditions aux limites :

Cellules du bas (j=0) : $ \theta=1$
$$ \Delta x \Delta y \left(\theta_{i,j}^{n+1} - \theta_{i,j}^{n}\right) = \Delta t \left[ \Delta_y \left(\theta_{i-1/2,j}^n - \theta_{i+1/2,j}^n\right) 
	+ \frac{\Delta y}{Pe \Delta x } \left(\theta_{i-1,j}^n - 2\theta_{i,j}^n + \theta_{i+1,j}^n\right)    + \frac{\Delta x}{Pe \Delta y } \left( 1/2-3\theta{i,j})/2 +  \theta_{i,j+1}^n\right) \right]
$$

$$ \theta_{i,j}^{n+1} =  \theta_{i,j}^{n} + \Delta t \left[ \frac{1}{2\Delta x} \left(\theta_{i-1,j}^n - \theta_{i+1,j}^n\right) 
	+ \frac{1}{Pe \Delta x^2 } \left(\theta_{i-1,j}^n - 2\theta_{i,j}^n + \theta_{i+1,j}^n\right)    + \frac{1}{Pe \Delta y^2 } \left(1/2 - 1.5\theta_{i,j}^n + \theta_{i,j+1}^n\right) \right]
$$


Cellules de gauche (i=0) : $ \theta=0$
$$ \theta_{i,j}^{n+1} =  \theta_{i,j}^{n} + \Delta t \left[ -\frac{1}{2\Delta x} \left( \theta_{i,j}^n+\theta_{i+1,j}^n\right) 
	+ \frac{1}{Pe \Delta x^2 } \left( - 1.5\theta_{i,j}^n + \theta_{i+1,j}^n\right)    +  \frac{1}{Pe \Delta y^2 } \left(\theta_{i,j-1}^n - 2\theta_{i,j}^n + \theta_{i,j+1}^n\right) \right]
$$

Cellules du haut ($i=N_x$) : flux nul

$$ \theta_{i,j}^{n+1} =  \theta_{i,j}^{n} + \Delta t \left[ -\frac{1}{2\Delta x} \left( \theta_{i,j}^n+\theta_{i+1,j}^n\right) 
	+ \frac{1}{Pe \Delta x^2 } \left( - 1.5\theta_{i,j}^n + \theta_{i+1,j}^n\right)    +  \frac{1}{Pe \Delta y^2 } \left(\theta_{i,j-1}^n - 2\theta_{i,j}^n + \theta_{i,j+1}^n\right) \right]
$$


Pour la solution exacte (sans diffusion en $x$), on peut calculer  le nombre de Nusselt :
 
$$ Nu = \int_0^1  \frac{1}{\sqrt{\pi x/Pe}} d\t{x} $$ 
$$ Nu = 2\sqrt{Pe/\pi}$$ 


\end{document}